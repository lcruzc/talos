FROM ubuntu:16.04

# Ensure that Python outputs everything that's printed inside
# the application rather than buffering it.
ENV PYTHONUNBUFFERED 1

RUN apt-get update && \
    apt-get upgrade -y && \ 	
    apt-get install -y \
    git \
    python3 \
    python3-dev \
    python3-setuptools \
    python3-pip \
    nginx \
    supervisor \
    sqlite3 \
    curl \
    apt-transport-https && \
    pip3 install -U pip setuptools && \
    rm -rf /var/lib/apt/lists/*

RUN cat /etc/os-release
RUN curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add -
RUN curl https://packages.microsoft.com/config/ubuntu/16.04/prod.list > /etc/apt/sources.list.d/mssql-release.list
RUN apt-get -y update
RUN apt-get -y install vim
RUN ACCEPT_EULA=Y apt-get -y install msodbcsql
RUN ACCEPT_EULA=Y apt-get -y install mssql-tools
RUN apt-get -y install unixodbc-dev 

# install uwsgi now because it takes a little while
RUN pip3 install uwsgi


# setup all the configfiles
RUN echo "daemon off;" >> /etc/nginx/nginx.conf
COPY nginx-app.conf /etc/nginx/sites-available/default
COPY supervisor-app.conf /etc/supervisor/conf.d/

COPY requirements.txt /home/docker/code/app/
RUN pip3 install -r /home/docker/code/app/requirements.txt

# add (the rest of) our code
COPY . /home/docker/code/

EXPOSE 80 443

CMD ["supervisord", "-n"]