from payroll.payroll_accounting import PayRollAccoutingStep, MissingDataException

class PayRollBaseSalary(PayRollAccoutingStep):

    def validate(self):
        self.validate_data(["salary", "minimum_salary"])


    def get_extra_hour_value(self):
        return self.get_normal_hour_value() * 1.5

    def get_normal_hour_value(self):
        return round(((self.context.get("salary")/30)*7)/45)

    def get_day_value(self):
        return self.context.get("salary")/30

    def get_gratification_limit(self):
        return self.context.get("minimum_salary") * 4.75

    def do_execute(self):
        
        return {
            "normal_hour_value" : self.get_normal_hour_value(),
            "extra_hour_value" : self.get_extra_hour_value(),
            "day_value" : self.get_day_value(),
            "gratification_limit" : self.get_gratification_limit()
        }

class PayRollTaxableAssets(PayRollAccoutingStep):
    
    def validate(self): 
        self.validate_data(["day_value", "work_by_hour", "normal_hour_value", "worked_days", "gratification_limit", "extra_hour_value"])

    def get_taxable_salary(self):
        if self.context.get("work_by_hour"):
            return round(self.context.get("normal_hour_value") * self.context.get("worked_days"))
        else:
            return round(self.context.get("day_value") * self.context.get("worked_days"))
    
    def get_gratification(self):
        gratification = round(self.context.get("salary")* 0.25)
        annual_value = gratification * 12;     
        return round(self.context.get("gratification_limit")/12) if annual_value >= self.context.get("gratification_limit") else gratification

    def get_taxable_assets(self):
        return self.get_taxable_salary() + self.get_gratification()

    def do_execute(self):

        return {
            "gratification" : self.get_gratification(),
            "taxable_salary" : self.get_taxable_salary(),
            "taxable_assets" : self.get_taxable_assets()
        }

class PayRollNoTaxableAssets(PayRollAccoutingStep):
    pass

class PayRollHealthContributions(PayRollAccoutingStep):
    """
    Calculate health contributions
    """
    def validate(self): 
        self.validate_data(["total_assets", "minimum_health_discount"])
        self.total_assets = self.context.get("total_assets")
        self.minimum_health_discount = self.context.get("minimum_health_discount")

    def do_execute(self):
        health_difference = 0
        health_contribution = public_health_contribution = self.total_assets * self.minimum_health_discount

        if not self.user.is_public_health:
            private_health_contribution = self.total_assets  * self.user.health_institution.discount_percentage
            if private_health_contribution > public_health_contribution:
                health_contribution = private_health_contribution
                health_difference = private_health_contribution - public_health_contribution

        return {
            "health_contribution" : health_contribution,
            "health_difference" : health_difference
        }

class PayRollRetirementProvision(PayRollAccoutingStep):

    def validate(self):
        self.validate_data(["total_assets"])
        self.total_assets = self.context.get("total_assets")

    def do_execute(self):
        return {
            "retirement_provision" : self.user.retirement_provision_institution.discount_percentage * self.total_assets
        }



class PayRollUnemploymentInsurance(PayRollAccoutingStep):

    def validate(self):
        self.validate_data(["total_assets", "unemployment_insurance_limit", "unemployment_company_tax", "unemployment_employee_tax"])
        self.total_taxable_income = self.context.get("total_assets")
        self.unemployment_insurance_limit = self.context.get("unemployment_insurance_limit")
        self.unemployment_company_tax = self.context.get("unemployment_company_tax")
        self.unemployment_employee_tax = self.context.get("unemployment_employee_tax")

    def do_execute(self):
        employee_contribution = self.total_taxable_income * self.unemployment_employee_tax
        company_contribution = self.total_taxable_income * self.unemployment_company_tax

        if not user.contract_type == "fixed":
            company_contribution = company_contribution + employee_contribution
            employee_contribution = 0

        return {  
            "unemployment_insurance_contribution" : {
                "company" : company_contribution,
                "employee" : employee_contribution
            }
        }