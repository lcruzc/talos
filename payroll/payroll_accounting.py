import functools
import pprint
from django.utils.module_loading import import_string

class MissingDataException(Exception):
    pass

PAYROLL_STEPS = [
    'payroll.payroll_steps.PayRollBaseSalary',
    'payroll.payroll_steps.PayRollTaxableAssets',
    #'payroll.payroll_steps.PayRollNoTaxableAssets',
]

class PayRollAccoutingStep():
    
    def __init__(self, user, context):
        self.user = user
        self.context = context

    def validate_data(self, fields):
        for field in fields:
            if not field in self.context:
                print (self.context)
                raise MissingDataException(field)

    def execute(self):
        self.validate()
        return self.do_execute()


class PayRollAccouting():

    def __init__(self, user, initial_context = None):
        self.user = user
        self.context = {}

        if initial_context:
            self.context.update(initial_context)

    def execute(self):
        
        context = self.context
        context.update({
            "salary" : self.user.salary
        })

        for payroll_step_path in PAYROLL_STEPS:
            payroll_step = import_string(payroll_step_path)
            
            try:
                ps_instance = payroll_step(self.user, context)
                context.update(ps_instance.execute())
            except MissingDataException as e:
                raise e

        pp = pprint.PrettyPrinter(depth=6)
        pp.pprint(context)

