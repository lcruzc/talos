from django.test import TestCase
from payroll.payroll_accounting import PayRollAccouting
from payroll.payroll_steps import PayRollHealthContributions, PayRollRetirementProvision

class HealthInstitution():
    
    def __init__(self, discount_percentage):
        self.discount_percentage = discount_percentage
    
class RetirementProvisionInstitution():
    
    def __init__(self, discount_percentage):
        self.discount_percentage = discount_percentage

class User():
    health_institution = HealthInstitution(0.07)
    salary = 4714754
    work_by_hour = False
    is_public_health = True
    is_open_ended_contract = True

class PayrollAccountingTest(TestCase):
    
    def test_payroll(self):

        

        payroll_accouting = PayRollAccouting(user, {
            "work_by_hour" : user.work_by_hour,
            "worked_days" : 30,
            "minimum_salary" : 301000
        })
        payroll_accouting.execute()

    def test_health_public_contributions(self):
        user = User()

        context = {
            "total_assets" : 1000000,
            "minimum_health_discount" : 0.07
        }
        step = PayRollHealthContributions(user, context)
        new_context = step.execute()
        self.assertEquals(int(new_context.get("health_contribution")), 70000)

    def test_health_private_contributions(self):
        user = User()
        user.health_institution = HealthInstitution(0.08)
        user.is_public_health = False

        context = {
            "total_assets" : 1000000,
            "minimum_health_discount" : 0.07
        }
        step = PayRollHealthContributions(user, context)
        new_context = step.execute()
        self.assertEquals(int(new_context.get("health_contribution")), 80000)
        self.assertEquals(int(new_context.get("health_difference")), 10000)

    def test_retirement_provision(self):
        user = User()
        user.retirement_provision_institution = RetirementProvisionInstitution(0.1144)

        context = {
            "total_assets" : 1000000
        }

        step = PayRollRetirementProvision(user, context)
        new_context = step.execute()
        self.assertEquals(new_context.get("retirement_provision"), 114400)