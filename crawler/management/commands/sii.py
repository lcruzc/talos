
from django.core.management.base import BaseCommand, CommandError
from uuid import uuid4

import datetime
import json
import requests



class Command(BaseCommand):
    help = 'Retrive documents from SII'

    def add_arguments(self, parser):
        parser.add_argument('year', type=int)

    def get_year(self, year):
        today = datetime.datetime.today()
        month = 12
        if today.year == year:
            month = today.month 
        return (["%s%s" % (year, str(x).zfill(2)) for x in range(1, month + 1)])


    def handle(self, *args, **options):
        periods = self.get_year(options['year'])
        session = requests.Session()

        data = {
            'clave' : '2b20192',
            'dv' : '6',
            'referencia' : 'https://misiir.sii.cl/cgi_misii/siihome.cgi',
            'rut' : '76262174',
            'rutcntr' : '76.262.174-6'
        }

        request = session.post('https://zeusr.sii.cl/cgi_AUT2000/CAutInicio.cgi', data = data)
        token = request.cookies.get('TOKEN')

        for period in periods:
            self.make_requests(session, token, period)

    def make_requests(self, session, token, period):
        print (period)
        uuid = str(uuid4())

        metadata = {
            "metaData": { 
                "namespace" : "cl.sii.sdi.lob.diii.consdcv.data.api.interfaces.FacadeService/getDetalleCompraExport",
                "conversationId" : token, 
                "transactionId" : uuid,
                "page" : None,
            },
            "data" : { 
                "rutEmisor" : "76262174",
                "dvEmisor" : "6",
                "ptributario" : str(period),
                "codTipoDoc" : 0,
                "operacion" : "COMPRA", 
                "estadoContab" : "REGISTRO"
            }
        }

        headers = {'Content-type': 'application/json', 'Accept': 'application/json, text/plain, */*'}
        request = session.post('https://www4.sii.cl/consdcvinternetui/services/data/facadeService/getDetalleCompraExport', 
                        data=json.dumps(metadata), headers=headers)

        print (request.content) 